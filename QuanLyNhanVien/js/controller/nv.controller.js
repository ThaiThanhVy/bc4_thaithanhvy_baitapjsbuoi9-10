function layThongTinTuForm() {
  const taiKhoan = document.getElementById("tknv").value;
  const hoTen = document.getElementById("name").value;
  const email = document.getElementById("email").value;
  const matKhau = document.getElementById("password").value;
  const ngayThang = document.getElementById("datepicker").value;
  const luongCB = document.getElementById("luongCB").value;
  const chucVu = document.getElementById("chucvu").value;
  const gioLam = document.getElementById("gioLam").value;

  let nv = new NhanVien(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayThang,
    luongCB,
    chucVu,
    gioLam
  );
  return nv;
}
function renderDSNV(nvArr) {
  // tạo chuỗi content html rỗng
  let contentHTML = "";
  nvArr.forEach((nv) => {
    let contentTr = `
    <tr>
        <td>${nv.taiKhoan}</td>
        <td>${nv.ten}</td>
        <td>${nv.email}</td>
        <td>${nv.ngaythang}</td>
        <td>${nv.chucvu}</td>
        <td>${nv.tinhTongLuong()}</td>
        <td>${nv.xepLoaiNV()}</td>
        <td>    
          <button onclick=xoaNhanVien('${
            nv.taiKhoan
          }') class="btn btn-danger mb-2">Xóa</button>
          <button class="btn btn-warning">Sửa</button>
        </td>
    </tr>
    `;
    contentHTML += contentTr;
  });
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

