function NhanVien(
  _taikhoan,
  _ten,
  _email,
  _matkhau,
  _ngaythang,
  _luong,
  _chucvu,
  _giolam
) {
  this.taiKhoan = _taikhoan;
  this.ten = _ten;
  this.email = _email;
  this.matkhau = _matkhau;
  this.ngaythang = _ngaythang;
  this.luong = _luong;
  this.chucvu = _chucvu;
  this.giolam = _giolam;
  this.tinhTongLuong = function () {
    var tongLuong = 0;
    if (this.chucvu == "Sếp") {
      tongLuong = this.luong * 3;
    } else if (this.chucvu == "Trưởng phòng") {
      tongLuong = this.luong * 2;
    } else {
      tongLuong = this.luong
    }
    return tongLuong;
  };
  this.xepLoaiNV = function () {
    if (this.giolam >= 192) {
      return "Nhân viên xuất sắc";
    } else if (this.giolam >= 176) {
      return "Nhân viên giỏi";
    } else if (this.giolam >= 160) {
      return "Nhân viên khá";
    } else {
      return "Nhân viên trung bình";
    }
  };
}


function renderDSNV(nvArr) {
  var contentHTML = "";
  for (var i = 0; i < nvArr.length; i++) {
    var nv = nvArr[i];
    var trContent = ` <tr>
    <td>${nv.taiKhoan}</td>
    <td>${nv.ten}</td>
    <td>${nv.email}</td>
    <td>${nv.ngaythang}</td>
    <td>${nv.chucvu}</td>
    <td>${nv.tinhTongLuong()}</td>
    <td>${nv.xepLoaiNV()}</td>
    <td>
    <button onclick="xoaSinhVien('${nv.taiKhoan}')" class="btn btn-danger">Xoá</button>
    <button 
    onclick="suaNhanVien('${nv.taiKhoan}')"
    class="btn btn-warning" data-toggle="modal" data-target="#myModal">Sửa</button>
    </td>
    </tr>`;
    contentHTML += trContent;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}


function timKiemViTri(id, dsnv) {
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    if (nv.taiKhoan == id) {
      return index;

    }
  }
  return -1;
}


function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngaythang;
  document.getElementById("luongCB").value = nv.luong;
  document.getElementById("chucvu").value = nv.chucvu;
  document.getElementById("gioLam").value = nv.giolam;
}
