const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

var dsnv = [];

var dsnvJson = localStorage.getItem("DSSV_LOCALSTORAGE");

if (dsnvJson != null) {
  dsnv = JSON.parse(dsnvJson);
  console.log("dsnv: ", dsnv);
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    // console.log("nv: ", nv);
    dsnv[index] = new NhanVien(
      nv.taiKhoan,
      nv.ten,
      nv.email,
      nv.matkhau,
      nv.ngaythang,
      nv.luong,
      nv.chucvu,
      nv.giolam
    );
  }
  renderDSNV(dsnv);
}

document.getElementById("btnThemNV").addEventListener("click", function () {
  console.log("yes");
  var newNV = layThongTinTuForm();

  //kiểm tra validation tài khoản
  let isValid =
    validator.kiemTraRong(
      newNV.taiKhoan,
      "tbTKNV",
      "Tài khoản không được để trống"
    ) &&
    validator.kiemTraTaiKhoan(
      newNV.taiKhoan,
      "tbTKNV",
      "Tài khoản chỉ được tối đa 4 đến 6 kí tự",
      4,
      6
    );
  //kiểm tra validation họ tên
  isValid &=
    validator.kiemTraRong(
      newNV.ten,
      "tbTen",
      "Tên nhân viên không được để trống"
    ) &&
    validator.kiemtraTenNV(
      newNV.ten,
      "tbTen",
      "Tên nhân viên phải là chữ"
    );
  // kiểm tra validation email
  isValid &=
    validator.kiemTraRong(newNV.email, "tbEmail", "Chưa nhập Email") &&
    validator.kiemtraEmail(
      newNV.email,
      "tbEmail",
      "Email không đúng định dạng"
    );
  // kiểm tra mật khẩu
  isValid &=
    validator.kiemTraRong(newNV.matkhau, "tbMatKhau", "Chưa nhập password") &&
    validator.kiemtraPass(
      newNV.matkhau,
      "tbMatKhau",
      "Mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );
  isValid &= validator.kiemTraRong(
    newNV.ngaythang,
    "tbNgay",
    "Ngày tháng không được để trống"
  );
  isValid &=
    validator.kiemTraRong(newNV.luong, "tbLuongCB", "Chưa nhập lương") &&
    validator.kiemTraLuong(
      newNV.luong,
      "tbLuongCB",
      "Lương phải từ 1,000,000VND-20,000,000VND",
      1000000,
      20000000
    );
  isValid &= validator.kiemtraChucVu(
    newNV.chucvu,
    "tbChucVu",
    "Chưa chọn chức vụ hợp lệ! Vui lòng nhập lại"
  );
  isValid &=
    validator.kiemTraRong(newNV.giolam, "tbGiolam", "Chưa nhập giờ làm") &&
    validator.kiemtraGioLam(
      newNV.giolam,
      "tbGiolam",
      "Nhập sai!Số giờ làm trong tháng phải từ 80-200 giờ",
      80,
      200
    );
  // console.log("newNV: ", newNV);
  if (isValid) {
    dsnv.push(newNV);
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem("DSSV_LOCALSTORAGE", dsnvJson);
    renderDSNV(dsnv);
  }
});

function xoaSinhVien(id) {
  var index = timKiemViTri(id, dsnv);
  if (index != -1) {
    dsnv.splice(index, 1);
    renderDSNV(dsnv);
  }
}

function suaNhanVien(id) {
  var index = timKiemViTri(id, dsnv);
  if (index != -1) {
    var nv = dsnv[index]
    showThongTinLenForm(nv)
  }
}


document.getElementById("btnCapNhat").addEventListener("click", function () {
  var updateNV = layThongTinTuForm();

  //kiểm tra validation tài khoản
  let isValid =
    validator.kiemTraRong(
      updateNV.taiKhoan,
      "tbTKNV",
      "Tài khoản không được để trống"
    ) &&
    validator.kiemTraTaiKhoan(
      updateNV.taiKhoan,
      "tbTKNV",
      "Tài khoản chỉ được tối đa 4 đến 6 kí tự",
      4,
      6
    );
  //kiểm tra validation họ tên
  isValid &=
    validator.kiemTraRong(
      updateNV.ten,
      "tbTen",
      "Tên nhân viên không được để trống"
    ) &&
    validator.kiemtraTenNV(
      updateNV.ten,
      "tbTen",
      "Tên nhân viên phải là chữ"
    );
  // kiểm tra validation email
  isValid &=
    validator.kiemTraRong(updateNV.email, "tbEmail", "Chưa nhập Email") &&
    validator.kiemtraEmail(
      updateNV.email,
      "tbEmail",
      "Email không đúng định dạng"
    );
  // kiểm tra mật khẩu
  isValid &=
    validator.kiemTraRong(updateNV.matkhau, "tbMatKhau", "Chưa nhập password") &&
    validator.kiemtraPass(
      updateNV.matkhau,
      "tbMatKhau",
      "Mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );
  isValid &= validator.kiemTraRong(
    updateNV.ngaythang,
    "tbNgay",
    "Ngày tháng không được để trống"
  );
  isValid &=
    validator.kiemTraRong(updateNV.luong, "tbLuongCB", "Chưa nhập lương") &&
    validator.kiemTraLuong(
      updateNV.luong,
      "tbLuongCB",
      "Lương phải từ 1,000,000VND-20,000,000VND",
      1000000,
      20000000
    );
  isValid &= validator.kiemtraChucVu(
    updateNV.chucvu,
    "tbChucVu",
    "Chưa chọn chức vụ hợp lệ! Vui lòng nhập lại"
  );
  isValid &=
    validator.kiemTraRong(updateNV.giolam, "tbGiolam", "Chưa nhập giờ làm") &&
    validator.kiemtraGioLam(
      updateNV.giolam,
      "tbGiolam",
      "Nhập sai!Số giờ làm trong tháng phải từ 80-200 giờ",
      80,
      200
    );

  if (isValid) {
    var taiKhoanNV = updateNV.taiKhoan;
    var index = timKiemViTri(taiKhoanNV, dsnv);
    if (index != 1) {
      dsnv[index] = updateNV;
      localStorage.setItem("DSSV_LOCALSTORAGE", JSON.stringify(dsnv));
      renderDSNV(dsnv);
    }
  }

  document.getElementById("tknv").disabled = false;
})


function searchUser() {
  let valueSearchInput = document.getElementById("searchName").value;

  let userSearch = dsnv.filter(value => {

    return value.xepLoaiNV().toUpperCase().includes(valueSearchInput.toUpperCase())
  })
  renderDSNV(userSearch);
}

